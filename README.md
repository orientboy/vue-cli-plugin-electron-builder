# Vue CLI Plugin Electron Builder

> Easily Build Your Vue.js App For Desktop With Electron


## Quick Start:

Open a terminal in the directory of your app created with Vue-CLI 3 or 4 (4 is recommended).

Then, install and invoke the generator of vue-cli-plugin-electron-builder by running:

`vue add electron-builder`

That's It! You're ready to go!

### To start a development server:

If you use [Yarn](https://yarnpkg.com/en/):

`yarn electron:serve`

or if you use NPM:

`npm run electron:serve`

### To build your app:

With Yarn:

`yarn electron:build`

or with NPM:

`npm run electron:build`

```js
// Demo
electronBuilder: {
    preload: "./app/preload.ts",
    nodeIntegration: true,
    mainProcessFile: './app/main.ts',  // 设置应用主进程的入口
    mainProcessWatch: ['./app/main.ts'],
    configureWebpack: {
        devtool: 'eval-source-map',
    },
    builderOptions: {
        appId: process.env.VUE_APP_APPID,
        productName: process.env.VUE_APP_PRODUCTNAME,
        copyright: "Copyright ©2023 www.tobog.cn", //版权信息
        extraMetadata: {
            name: process.env.VUE_APP_APPID.split('.').pop(),
            version: process.env.VUE_APP_VERSION
        },
        // files: ['./**/*'],
        // extraFiles: [ // 把指定的资源复制到程序根目录，
        //     {
        //         from: './public',
        //         to: './'
        //     }
        // ],
        // extraFiles: [
        //     {
        //         from: 'postinst',
        //         to: 'postinst',
        //         // toType: 'file',
        //         // mode: 0o755,
        //     },
        // ],
        asar: true,
        electronDownload: {
            mirror: "https://npm.taobao.org/mirrors/electron/"
        },
        directories: {
            output: "dist_electron",
            buildResources: "build",
            app: "dist_electron/bundled"
        },
        mac: {
            target: "dmg",
            icon: "public/icons/icon.icns",
        },
        win: {
            artifactName: 'webdesk_setup_1.0.${ext}',
            icon: "public/icons/icon.ico",
            sign: null,
            target: [
                {
                    target: "nsis", //利用nsis制作安装程序,打包文件的后缀为exe
                    arch: [
                        "x64", //64位
                        "ia32" //32位
                    ]
                }
            ],
        },
        nsis: {
            oneClick: false, //一键安装
            language: "2052", //安装语言
            perMachine: true, //应用所有用户
            warningsAsErrors: false,
            allowElevation: true, // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
            allowToChangeInstallationDirectory: true, // 允许修改安装目录
            installerIcon: "public/icons/icon.ico", // 安装图标
            uninstallerIcon: "public/icons/icon.ico", // 卸载图标
            installerHeaderIcon: "public/icons/icon.ico", // 安装时头部图标
            createDesktopShortcut: true, // 创建桌面图标
            createStartMenuShortcut: true, // 创建开始菜单图标
            shortcutName: process.env.VUE_APP_PRODUCTNAME // 图标名称(项目名称)
        },
        linux: {
            icon: "public/icons",
            target: ["deb"],
        },
        deb: {
            // depends: ['xvfb'],
            // 安装包后执行的脚本的路径，可用于自定义安装逻辑。
            afterInstall: './postinst.sh',
            // 移除包后执行的脚本的路径，可用于自定义卸载逻辑。
            afterRemove: void 0
        }
    },
    chainWebpackMainProcess(config) {
        config.resolve.extensions.add('.node');
        config.module
            .rule('node')
            .test(/\.node$/)
            .use('node-loader')
            .loader('node-loader')
            .end();
        config.module
            .rule('ts')
            .test(/\.tsx?$/)
            .use('ts-loader')
            .loader('ts-loader')
            .end();
        config.module.rule('jsx').test(/\.jsx?$/).use('jsx').loader('babel-loader');
    },
}
```
